val Dependencies = new {
  val Version = new {
    val cats = "3.2.9"
    val refined = "0.9.27"
    val catsEffectMunit = "1.0.6"
  }

  val catsEffect = "org.typelevel" %% "cats-effect" % Version.cats
  val catsEffectKernel = "org.typelevel" %% "cats-effect-kernel" % Version.cats
  val catsEffectStd = "org.typelevel" %% "cats-effect-std" % Version.cats
  val refined = "eu.timepit" %% "refined" % Version.refined
  val refinedCats = "eu.timepit" %% "refined-cats" % Version.refined
  val catsEffectMunit = "org.typelevel" %% "munit-cats-effect-3" % Version.catsEffectMunit
}

lazy val root = (project in file(".")).settings(
  name := "swedish_identity_number_validator",
  organization := "bzh.ya2o",
  scalaVersion := "3.1.0",
  Global / cancelable := true,
  libraryDependencies ++= Seq(
    Dependencies.catsEffect,
    Dependencies.catsEffectKernel,
    Dependencies.catsEffectStd,
    Dependencies.refined,
    Dependencies.catsEffectMunit % Test
  )
)

addCommandAlias("validate", "clean; test; scalafmtCheck; scalafmtSbtCheck; doc")
