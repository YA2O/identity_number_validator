package bzh.ya2o

import cats.effect.Sync
import scala.io.StdIn

trait Console[F[_]] {
  def readLine(): F[String]
  def writeLine(s: String): F[Unit]
}

class ConsoleImpl[F[_]]()(implicit F: Sync[F]) extends Console[F] {
  override def readLine(): F[String] = F.delay(StdIn.readLine())
  override def writeLine(s: String): F[Unit] = F.delay(print(s))
}
