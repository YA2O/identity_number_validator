package bzh

import cats.data.NonEmptyChain

package object ya2o {
  type ValidationErrors = NonEmptyChain[String]

  implicit class AnyX[X](val x: X) extends AnyVal {
    def |>[Y](f: X => Y): Y = f(x)
  }
}
