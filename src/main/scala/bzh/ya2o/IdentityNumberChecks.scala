package bzh.ya2o

import cats.data.NonEmptyChain
import cats.implicits.*
import scala.util.matching.Regex
import java.time.LocalDate
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import scala.util.Try
import scala.util.control.NonFatal

object IdentityNumberChecks {

  val PersonalIdentityNumberValidityCheck = ValidityCheck(
    FormatValidityCheck("""^(18|19|20)?\d{6}([-+]?)\d{4}$""".r),
    ControlDigitValidityCheck,
    DateValidityCheck(identity)
  )

  val CoordinationNumberValidityCheck = ValidityCheck(
    FormatValidityCheck("""^(18|19|20)?\d{6}([-+]?)\d{4}$""".r),
    ControlDigitValidityCheck,
    DateValidityCheck((s: String) => (s.toInt - 60).toString)
  )

  val OrganisationNumberValidityCheck = ValidityCheck(
    FormatValidityCheck("""^(16)?\d{6}([-]?)\d{4}$""".r),
    ControlDigitValidityCheck,
    MiddlePairBiggerThan20
  )

  private class FormatValidityCheck(regex: Regex) extends ValidityCheck {
    override def validate(s: String): Either[ValidationErrors, Unit] = {
      if (regex.matches(s)) Either.right(())
      else Either.left(NonEmptyChain.one(s"[$s] doesn't have a valid format!"))
    }
  }

  private object ControlDigitValidityCheck extends ValidityCheck {
    override def validate(s: String): Either[ValidationErrors, Unit] = {

      def getControlDigit(s: String): Int = {
        s.last.asDigit
      }

      // Luhn's algorithm
      def computeControlDigit(s: String): Int = {
        val nineDigits: String = s.replace("-", "").replace("+", "").takeRight(10).dropRight(1)
        val j: Int = nineDigits
          .map(_.asDigit)
          .zipWithIndex
          .map { case (digit, index) =>
            if (index % 2 == 0) digit * 2 else digit
          }
          .flatMap { n =>
            List(n / 10, n % 10)
          }
          .sum
        if (j % 10 == 0) 0
        else (10 - (j % 10)) % 10
      }

      def isValid(s: String): Boolean = {
        getControlDigit(s) == computeControlDigit(s)
      }

      try {
        if (isValid(s)) Either.right(())
        else Either.left(NonEmptyChain.one(s"[$s] doesn't have a valid control digit!"))
      } catch {
        case NonFatal(_) =>
          Either.left(NonEmptyChain.one(s"Cannot check validity of control digit for [$s]!"))
      }
    }
  }

  private class DateValidityCheck(transformDateDigitsBeforeDateValidation: String => String)
    extends ValidityCheck {

    // TODO inject clock as a dependency!
    def getCurrentYear(): Int = LocalDate.now(ZoneId.of("Europe/Stockholm")).getYear

    override def validate(s: String): Either[ValidationErrors, Unit] = {
      def validate_(s: String): Either[ValidationErrors, Unit] = {

        def addLeading2YearDigits(sixDigits: String, moreThan100Years: Boolean = false): String = {
          require(sixDigits.length == 6, sixDigits)
          val currentYear: Int = getCurrentYear()
          val year =
            if (moreThan100Years) currentYear - 100
            else currentYear

          if (sixDigits.take(2).toInt > (year % 100)) s"${year / 100 - 1}$sixDigits"
          else s"${year / 100}$sixDigits"
        }

        def validateDate(eightDigits: String): Either[ValidationErrors, Unit] = {
          require(eightDigits.length == 8, eightDigits)

          def parseDate(eightDigits: String): Either[ValidationErrors, LocalDate] = {
            Try {
              LocalDate.parse(eightDigits, DateTimeFormatter.BASIC_ISO_DATE)
            }.toEither.leftMap { t =>
              NonEmptyChain.one(s"[$eightDigits] is not a valid date!")
            }
          }

          parseDate(eightDigits).as(())
        }

        val eightDigitsAndMaybeHyphen = """^(\d{8})-?$""".r
        val sixDigitsAndMaybeHyphen = """^(\d{6})-?$""".r
        val sixDigitsAndPlus = """^(\d{6})\+$""".r
        (s.dropRight(4) match {
          case eightDigitsAndMaybeHyphen(digits: String) =>
            Either.right(digits)
          case sixDigitsAndMaybeHyphen(digits: String) =>
            Either.right(addLeading2YearDigits(digits))
          case sixDigitsAndPlus(digits: String) =>
            Either.right(addLeading2YearDigits(digits, moreThan100Years = true))
          case _ =>
            Either.left(NonEmptyChain.one(s"[$s] doesn't have a date format handled by the program!"))

        }).flatMap { eightDigits =>
          eightDigits |> transformDateDigitsBeforeDateValidation |> validateDate
        }
      }

      try {
        validate_(s)
      } catch {
        case NonFatal(_) => Either.left(NonEmptyChain.one(s"Cannot check date validity for [$s]!"))
      }
    }
  }

  private object MiddlePairBiggerThan20 extends ValidityCheck {
    override def validate(s: String): Either[ValidationErrors, Unit] = {
      def isValid(s: String): Boolean = {
        s.slice(2, 4).toInt >= 20
      }

      try {
        if (isValid(s)) Either.right(())
        else Either.left(NonEmptyChain.one(s"[$s] doesn't have a valid middle digit pair!"))
      } catch {
        case NonFatal(_) =>
          Either.left(NonEmptyChain.one(s"Cannot check validity of middle digit pair for [$s]!"))
      }
    }
  }

}
