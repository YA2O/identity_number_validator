package bzh.ya2o

import cats.implicits.*

trait ValidityCheck {
  def validate(s: String): Either[ValidationErrors, Unit]
}

object ValidityCheck {

  def apply(predicates: ValidityCheck*): ValidityCheck = { (s: String) =>
    val (errors: List[ValidationErrors], _) = predicates.toList.map(_.validate(s)).separate
    if (errors.isEmpty) Either.right(())
    else Either.left(errors.reduce(_.concat(_)))
  }

  def showResult(checkResult: Either[ValidationErrors, Unit]): String = {
    checkResult.fold(
      errors => s"Validation error(s):\n  - ${errors.iterator.mkString("\n  - ")}",
      _ => "OK"
    )
  }

}
