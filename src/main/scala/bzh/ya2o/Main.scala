package bzh.ya2o

import bzh.ya2o.IdentityNumberChecks.*
import cats.effect.IO
import cats.effect.IOApp

object Main extends IOApp.Simple {

  def run: IO[Unit] = {
    val console: Console[IO] = new ConsoleImpl[IO]()

    (
      for {
        _ <- console.writeLine("------------------------\n")
        _ <- console.writeLine("> Enter input:\n")
        input <- console.readLine()
        pnCheckResult <- IO.pure(PersonalIdentityNumberValidityCheck.validate(input))
        _ <- console.writeLine(s"> Personal identity number?\n${ValidityCheck.showResult(pnCheckResult)}\n")
        cnCheckResult <- IO.pure(CoordinationNumberValidityCheck.validate(input))
        _ <- console.writeLine(s"> Coordination number?\n${ValidityCheck.showResult(cnCheckResult)}\n")
        onCheckResult <- IO.pure(OrganisationNumberValidityCheck.validate(input))
        _ <- console.writeLine(s"> Organisation number?\n${ValidityCheck.showResult(onCheckResult)}\n")
      } yield ()
    ).foreverM
  }

}
