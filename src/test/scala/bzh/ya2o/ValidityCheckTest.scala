package bzh.ya2o

import bzh.ya2o.IdentityNumberChecks.*
import munit.FunSuite

class ValidityCheckTest extends FunSuite {

  val validPersonalIdentityNumbers = List(
    "201701102384",
    "141206-2380",
    "20080903-2386",
    "7101169295",
    "198107249289",
    "19021214-9819",
    "190910199827",
    "191006089807",
    "192109099180",
    "4607137454",
    "194510168885",
    "900118+9811",
    "189102279800",
    "189912299816",
    "910227+9800"
  )

  val invalidPersonalIdentityNumbers = List(
    "",
    "abcdefghij",
    "201701272394",
    "190302299813",
    "194502308885", // February 30th
    "179102279800", //18th century
    "194513168885" // month 13
  )

  test("validate valid personal identity numbers") {
    validPersonalIdentityNumbers.foreach { s =>
      val checkResult = PersonalIdentityNumberValidityCheck.validate(s)
      assert(
        checkResult.isRight,
        s"[$s] is valid, but it doesn't pass validation check! ${ValidityCheck.showResult(checkResult)}"
      )
    }
  }

  test("invalidate invalid personal identity numbers") {
    invalidPersonalIdentityNumbers.foreach { s =>
      val checkResult = PersonalIdentityNumberValidityCheck.validate(s)
      assert(checkResult.isLeft, s"[$s] is invalid, but it does pass validation check!")
    }
  }

  val validCoordinationNumbers = List(
    "190910799824"
  )

  test("validate valid coordination numbers") {
    validCoordinationNumbers.foreach { s =>
      val checkResult = CoordinationNumberValidityCheck.validate(s)
      assert(
        checkResult.isRight,
        s"[$s] is valid, but it doesn't pass validation check! ${ValidityCheck.showResult(checkResult)}"
      )
    }
  }

  val validOrganisationNumbers = List(
    "556614-3185",
    "16556601-6399",
    "262000-1111"
  )

  test("validate valid organisation numbers") {
    validOrganisationNumbers.foreach { s =>
      val checkResult = OrganisationNumberValidityCheck.validate(s)
      assert(
        checkResult.isRight,
        s"[$s] is valid, but it doesn't pass validation check! ${ValidityCheck.showResult(checkResult)}"
      )
    }
  }

  val invalidOrganisationNumbers = List(
    "551614-3185",
    "16156601-6398",
    "260000-1115"
  )

  test("invalidate invalid organisation numbers") {
    invalidOrganisationNumbers.foreach { s =>
      val checkResult = OrganisationNumberValidityCheck.validate(s)
      assert(
        checkResult.isLeft,
        s"[$s] is invalid, but it does pass validation check! ${ValidityCheck.showResult(checkResult)}"
      )
    }
  }
}
